from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory,Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
# Create your views here.
@login_required
def receipt(request):
    receipt_obj = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_key": receipt_obj,
    }
    return render(request, "receipts/list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
        context = {
            "rview_key" : form,
        }
        return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
    category_obj = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_key" : category_obj,
    }
    return render(request, "receipts/categories.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()
        context = {
            "cview_key" : form,
        }
        return render(request, "receipts/create_category.html", context)

@login_required
def account_list(request):
    account_obj = Account.objects.filter(owner=request.user)
    context = {
        "account_key" : account_obj
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
        context = {
            "aview_key" : form,
        }
        return render(request, "receipts/create_account.html", context)